FROM ubuntu

ARG UID=1000
ARG USER=ubuntu
ARG PASSWD=ubuntu

ENV TZ Asia/Tokyo

RUN apt update && apt upgrade -y 
RUN apt install -y language-pack-ja
RUN update-locale LANG=ja_JP.UTF-8
RUN apt install -y tzdata
RUN apt install -y sudo
RUN apt install -y curl 
RUN apt install -y wget 
RUN apt install -y python3 
RUN apt install -y python3-pip python3-venv 
RUN apt install -y gcc g++ 
RUN apt install -y zip unzip tar gzip 
RUN apt install -y sleuthkit 
RUN apt install -y binwalk 
RUN apt install -y gdb 
RUN apt install -y 
RUN apt install -y netcat 
RUN apt install -y ssh git
RUN apt install -y vim

RUN useradd -m --uid ${UID} --groups sudo ${USER} \
    && echo ${USER}:${PASSWD} | chpasswd

USER ubuntu
WORKDIR /home/${USER}
