# Ubuntu Docker Image with sudo

## Info

|param|value|
|:-:|:-:|
|default user|ubuntu|
|defailt password|ubuntu|
|admin|true|
|$HOME|/home/ubuntu|
|$SHELL|bash|

## Quick Start

### Build

```shell
sudo docker build --platform linux/amd64 -t myctf .

sudo docker run --platform linux/amd64 -it -p 30000:30000 --name CTF myctf
```

### Dockerhub
```sh
sudo docker pull docker pull unicatflower/ctf:x86_64

usdo docker run --platform linux/amd64 -it -p 30000:30000 --name CTF unicatflower/ctf:x86_64
```
##  Restart No-Interactive Mode

```shell
docker start CTF
```

## Restart Interactive Mode

```shell
docker start -i CTF
```

## SSH

```shell
docker start -i CTF

# In the container, change your shell by running `chsh`
# And edit /etc/ssh/sshd_config line 14 and 57
# e.g.)
# sudo vim /etc/ssh/sshd_config
# ~~~
# #Port 22 -> Port 30000
# ~~~
# #PasswordAuthentication yes -> PasswordAuthentication yes
#

sudo service ssh restart
```
Open another terminal and run `ssh ubuntu@localhost -p 30000`
